<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;
Use Helpers;

class ArticleController extends Controller
{

  public function showAllArticles(){
    return response()->json(Article::all());
  }

  public function showOneArticle($id){
    return response()->json(Article::find($id));
  }

  public function showAllCommentsOfArticles($id){
    $comments = Article::findOrFail($id) -> comments;
    return response()->json($comments);
  }


  public function create(Request $request){
    $this->validate($request, [
      'title' => 'required',
      'author' => 'required',
      'description' => 'required',
      'content' => 'required',
      'published_at' => 'required'
    ]);

    $article = Article::create($request->all());

    return response()->json($article, 201);
  }


  public function update($id, Request $request){
    $article = Article::findOrFail($id);
    $article->update($request->all());

    return response()->json($article, 200);
  }

  public function delete($id){
    Article::findOrFail($id)->delete();
    return response('Deleted Successfully', 200);
  }

  
}