<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
  public function showAllComments(){
    return response()->json(Comment::all());
  }

  public function create(Request $request)
  {
    $this->validate($request, [
      'author' => 'required',
      'body' => 'required',
      'article_id' => 'required'
    ]);

    $article = Comment::create($request->all());

    return response()->json($article, 201);
  }

}