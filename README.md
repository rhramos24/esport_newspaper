# Información API esport_newspaper

----

**API staging host:**    [https://esport-newspaper.herokuapp.com](https://esport-newspaper.herokuapp.com)

A continuación se detallan los enpoints de la aplicación desarrollada.

----
## Articles

**POST /api/articles**

crear un nuevo articulo.

**body**

    [
     {
       "title": "",
       "author": "",
       "description": "",
       "content": "",
       "published_at": ""
     }
    ]
**response**

    {
      "title": "Title example",
      "author": "Juan Perez",
      "description": "Little description of article",
      "content": "FULL notice",
      "published_at": "2018/10/17",
      "updated_at": "2018-10-17 22:23:34",
      "created_at": "2018-10-17 22:23:34",
      "id": 5
    }

**Validación**

Todos los campos son requeridos

**mensaje de error**
    
    {
      "title": [
        "The title field is required."
      ]
    }

**GET /api/articles**

listado de todos los articulos publicados en el periodico.

    [
      {
        "id": 1,
        "title": "GEN G, FUERA DE WORDLS 2018",
        "author": "Humberto Ramos",
        "description": "El campeon del 2017 queda fuera en fase de grupos.",
        "content": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\ntempor incididunt  est laborum.",
        "published_at": "2018-10-17 00:00:00",
        "created_at": "2018-10-17 16:16:43",
        "updated_at": "2018-10-17 16:16:43"
      },
      {
        "id": 2,
        "title": "Example2",
        "author": "Juan Perez",
        "description": "Little description",
        "content": "full content",
        "published_at": "0000-00-00 00:00:00",
        "created_at": "2018-10-17 17:11:20",
        "updated_at": "2018-10-17 17:11:20"
      }
    ]

**GET /api/articles/{id}**

Obtener articulo con un id especifico. 

    [
      {
        "id": 1,
        "title": "GEN G, FUERA DE WORDLS 2018",
        "author": "Humberto Ramos",
        "description": "El campeon del 2017 queda fuera en fase de grupos.",
        "content": "full content",
        "published_at": "2018-10-17 00:00:00",
        "created_at": "2018-10-17 16:16:43",
        "updated_at": "2018-10-17 16:16:43"
      }
    ]

**GET /api/articles/{id}/comments**

Obtiene todos los comentarios hechos en un articulo.

    [
      {
        "id": 1,
        "article_id": 1,
        "author": "Juan Perez",
        "body": "woow excelente información.",
        "created_at": "2018-10-17 16:28:55",
        "updated_at": "2018-10-17 16:28:55"
      },
      {
        "id": 2,
        "article_id": 1,
        "author": "Pablito diaz",
        "body": "Fake news.",
        "created_at": "2018-10-17 16:31:42",
        "updated_at": "2018-10-17 16:31:42"
      }
    ]



## Comments

**GET /api/comments**

Obtiene todos los comentarios.

    [
      {
        "id": 1,
        "article_id": 1,
        "author": "Juan Perez",
        "body": "woow excelente información.",
        "created_at": "2018-10-17 16:28:55",
        "updated_at": "2018-10-17 16:28:55"
      },
      {
        "id": 2,
        "article_id": 2,
        "author": "Pablito diaz",
        "body": "Fake news.",
        "created_at": "2018-10-17 16:31:42",
        "updated_at": "2018-10-17 16:31:42"
      }
    ]

**POST /api/comments**

**body**

    {
     "author": "Juan Ramos",
     "body": "Muy buena noticia",
     "article_id": "1"
    }

**response**

    {
      "author": "Juan Ramos",
      "body": "Muy buena noticia",
      "article_id": "1",
      "updated_at": "2018-10-17 22:32:26",
      "created_at": "2018-10-17 22:32:26",
      "id": 3
    }


**Validación**

Todos los campos son requeridos

**mensaje de error **
    
    {
      "author": [
        "The author field is required."
      ]
    }
